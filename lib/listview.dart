import 'package:flutter/material.dart';
import 'package:flutter_app/image.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MaterialApp app = MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('ListView示例'),
        ),
        body: _buildListView(),
        //     ListView(
        //   scrollDirection: Axis.vertical,
        //   children: List.generate(200, (index) {
        //     return Text(
        //       '$index',
        //       style: TextStyle(fontSize: 20.0),
        //     );
        //   }),
        // ),
      ),
    );
    return app;
  }

  ListView _buildListView() {
    return ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('favorite_border'),
          subtitle: Text("favorite_border_subtitle"),
          trailing: Icon(Icons.arrow_forward),
          onTap: () {
            Fluttertoast.showToast(
                msg: "单击事件",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black45,
                textColor: Colors.white,
                fontSize: 16.0);
          },
          onLongPress: () {
            Fluttertoast.showToast(
                msg: "长按事件",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black45,
                textColor: Colors.white,
                fontSize: 16.0);
          },
        ),
      ],
    );
  }
}
