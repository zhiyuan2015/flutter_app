import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  FlutterError.onError = (FlutterErrorDetails details) {
    reportErrorAndLog(details);
  };
  //参数是Widget类型
  runZoned(() {
    runApp(MyApp());
  }, zoneSpecification: ZoneSpecification(
      print: (Zone self, ZoneDelegate parent, Zone zone, String line) {
    collectLog(line);
  }), onError: (Object obj, StackTrace stack) {
    var details = makeDetails(obj, stack);
    reportError(details);
  });
}

void collectLog(String line) {}

void reportErrorAndLog(FlutterErrorDetails details) {}

void reportError(FlutterErrorDetails details) {}

FlutterErrorDetails makeDetails(Object obj, StackTrace stack) {
  //构建错误信息
  return null;
}

class MyStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MaterialApp materialApp = MaterialApp(
      //root Widget
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      //主题内容
      home: new ContextRoute(),
    );
    return materialApp;
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    //context是Widget的位置信息
    //脚手架有状态的Widget
    Scaffold scaffold = Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.title,
          style: new TextStyle(fontSize: 40),
        ), //title是一个Widget类型的参数
      ),
      body: new Center(
        child: new Column(
          //child会居中对齐
          mainAxisAlignment: MainAxisAlignment.center, //主轴方向，y轴方向
          crossAxisAlignment: CrossAxisAlignment.start, //交叉轴对齐方式
          children: <Widget>[
            new Text('You hash pushed the button this many times:'),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            )
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        //什么用法，直接用函数名调用？
        onPressed: () {
          //_incrementationCounter();
          // debugDumpApp();
          debugDumpRenderTree();
        }, //通知刷新，跟count有关的Widget会刷新
        tooltip: 'Increment',
        child: new Icon(Icons.add), //图标
      ),
    );
    return scaffold;
  }

  void _incrementationCounter() {
    setState(() {
      _counter++;
    });
  }
}

class Echo extends StatelessWidget {
  const Echo({Key key, @required this.text, this.backgroundColor: Colors.grey})
      : super();

  final String text;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: backgroundColor,
        child: Text(text),
      ),
    );
  }
}

class ContextRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Context测试"),
      ),
      body: Container(
        child: Builder(builder: (context) {
          Scaffold scaffold = context.ancestorWidgetOfExactType(Scaffold);
          print(scaffold);
          return (scaffold.appBar as AppBar).title;
        }),
      ),
    );
  }
}
