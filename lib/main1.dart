import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Container示例Demo'),
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            //color: Color(0xff00ff00),
            width: 200.0,
            height: 200.0,
            decoration: BoxDecoration(
              color: Color(0xffffff00),
              border: Border.all( //边框
                color: Color(0xffff0000),
                width: 10.0
              ),
              borderRadius: const BorderRadius.all(Radius.circular(30.0)) //圆角
            ),
            child: new Text(
              'Container',
              style: TextStyle(fontSize: 28.0),
            ),
            transform: Matrix4.rotationZ(-pi / 9), //绕Z轴逆时针旋转角度
          ),
        ),
      ),
    );
  }
}
