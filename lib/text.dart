import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Text示例'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center, //布局方向对齐方式
            crossAxisAlignment: CrossAxisAlignment.center, //布局方向的垂直方向对齐方式
            children: <Widget>[
              Text(
                '动脑学院啧啧啧',
                softWrap: true,
                style: TextStyle(
                    decoration: TextDecoration.underline,
                    decorationStyle: TextDecorationStyle.wavy //波浪线
                    ),
              ),
              SizedBox(
                height: 10.0,
              ),
              RichText(
                text: TextSpan(
                    text: '哈哈哈',
                    style: TextStyle(color: Color(0xffff0000), fontSize: 20.0),
                    children: <TextSpan>[
                      TextSpan(
                          text: '努力做一个优秀的人',
                          //..是级联的意思
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              String url = 'https://www.baidu.com';
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'error: $url';
                              }
                            })
                    ]),
              ),
              RaisedButton(
                onPressed: () {
                  log("RaiseButton onPressed");
                },
                child: Text('RaiseButton'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
